//
//  NTWebViewViewController.m
//  newTindar
//
//  Created by steve on 2019/2/18.
//  Copyright © 2019 handu. All rights reserved.
//

#import "NTWebViewViewController.h"
#import <WebKit/WebKit.h>

#define     IS_IPad                 (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define     IS_IPhone               (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define     IPHONEX                 (StatusBarHeight != 20)
#define     SCREEN_Width            ([[UIScreen mainScreen] bounds].size.width)
#define     SCREEN_Height           ([[UIScreen mainScreen] bounds].size.height)
#define     StatusBarHeight         ([UIApplication sharedApplication].statusBarFrame.size.height)
#define     NavigationHeight        (StatusBarHeight + 44)
#define     TabBarHeight            (StatusBarHeight == 44 ? 83 : 49)
#define     TopSafeAreaHeight       (StatusBarHeight - 20)
#define     BottomSafeAreaHeight    (TabBarHeight - 49)
#define     ZoomSize(S)             (S *(SCREEN_Width)/375)


@interface NTWebViewViewController ()<WKNavigationDelegate, WKUIDelegate,WKScriptMessageHandler> {
    UILabel *titleLb;
}

@property (nonatomic, assign) BOOL needPopToMain;
@property (nonatomic, strong) WKWebView *webView;
@property (nonatomic, strong) UIProgressView *progressView;
//@property (nonatomic, strong) JXMapNavigationView      *mapNavigationView;

@end

@implementation NTWebViewViewController



- (void) viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBar.hidden = YES;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
    [self setData];
}

- (void)setUI {
    self.view.backgroundColor = UIColor.whiteColor;
    
    UIView *navBgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, NavigationHeight)];;
    navBgView.backgroundColor = [UIColor colorWithRed:60/255.0 green:176/255.0 blue:105/255.0 alpha:1];
    [self.view addSubview:navBgView];
    
    UIButton *backBt = [[UIButton alloc] initWithFrame:CGRectMake(0, StatusBarHeight, ZoomSize(44), ZoomSize(43))];
    [navBgView addSubview:backBt];
    [backBt setImage:[UIImage imageNamed:@"nav_back_black"] forState:UIControlStateNormal];
    [backBt addTarget:self action:@selector(backBt) forControlEvents:UIControlEventTouchUpInside];
    
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]]];
    [self.view addSubview:self.webView];
    [self.webView addSubview:self.progressView];
    
}

- (void) setData {
}

#pragma mark - lazy methods
- (WKWebView *)webView {
    if (_webView == nil) {
        
        WKUserContentController* userContentController = [[WKUserContentController alloc] init];
        [userContentController addScriptMessageHandler:self name:@"app"];
        
        // 设置偏好设置
        WKWebViewConfiguration *config = [[WKWebViewConfiguration alloc] init];
        config.userContentController = userContentController;
        
        _webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, NavigationHeight , SCREEN_Width , SCREEN_Height - NavigationHeight ) configuration:config];
        _webView.navigationDelegate = self;
        
    }
    
    [_webView addObserver:self forKeyPath:@"estimatedProgress" options:NSKeyValueObservingOptionNew context:nil];
    [_webView addObserver:self forKeyPath:@"title" options:NSKeyValueObservingOptionNew context:nil];
    [_webView addObserver:self forKeyPath:@"URL" options:NSKeyValueObservingOptionNew context:nil];
    
    return _webView;
}

- (UIProgressView *)progressView {
    if (_progressView == nil) {
        _progressView = [[UIProgressView alloc] initWithFrame:CGRectMake(0, 0, NavigationHeight, 2)];
    }
    _progressView.progressViewStyle = UIProgressViewStyleBar;
    
    return _progressView;
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    
    if ([keyPath isEqualToString:@"estimatedProgress"]) {
        CGFloat progress = [change[NSKeyValueChangeNewKey] floatValue];
        [self.progressView setProgress:progress animated:YES];
        if(progress == 1.0) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.progressView setProgress:0.0 animated:NO];
            });
        }
    }
    
    else if ([keyPath isEqualToString:@"title"]) {
        titleLb.text = self.webView.title;
        
    }
}

#pragma mark - funtion
- (void) backBt {
   [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - WKScriptMessageHandler
- (void)userContentController:(WKUserContentController *)userContentController
      didReceiveScriptMessage:(WKScriptMessage *)message {
    
    NSDictionary *dic = message.body;
    NSLog(@"%@",dic[@"function"]);
    
    if ([dic[@"function"] isEqualToString:@"setTitle"] ) {
        titleLb.text = dic[@"title"];
    }
}

#pragma mark-- 加载wkwebview 用于推送数据
- (void)loadWebView:(NSString *)urlString {
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlString]]];
}

//如果不实现这个代理方法,默认会屏蔽掉打电话等url
- (void) webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    decisionHandler(WKNavigationActionPolicyAllow);
}

/// 2 页面开始加载
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation{
    
    //关于拨打电话时的调用问题
    NSString *path= [webView.URL absoluteString];
    NSString * newPath = [path lowercaseString];
    
    if ([newPath hasPrefix:@"sms:"] || [newPath hasPrefix:@"tel:"]) {
        UIApplication * app = [UIApplication sharedApplication];
        if ([app canOpenURL:[NSURL URLWithString:newPath]]) {
            [app openURL:[NSURL URLWithString:newPath]];
        }
        return;
    }
}

/// 4 开始获取到网页内容时返回
- (void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation {
}

/// 5 页面加载完成之后调用
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
}

/// 页面加载失败时调用
- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation{
}


@end
