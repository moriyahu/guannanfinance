//
//  NTWebViewViewController.h
//  newTindar
//
//  Created by steve on 2019/2/18.
//  Copyright © 2019 handu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NTWebViewViewController : UIViewController

@property (nonatomic, strong) NSString *url;        //request地址

@end

NS_ASSUME_NONNULL_END
