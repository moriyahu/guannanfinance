//
//  ViewController.m
//  GuanNanFinance
//
//  Created by steve on 2019/1/7.
//  Copyright © 2019 GuanNanFinance. All rights reserved.
//

//#define kDEVICEWIDTH  [UIScreen mainScreen].bounds.size.width
//#define kDEVICEHEIGHT  [UIScreen mainScreen].bounds.size.height

#define     IS_IPad                 (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define     IS_IPhone               (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define     IPHONEX                 (StatusBarHeight != 20)
#define     SCREEN_Width            ([[UIScreen mainScreen] bounds].size.width)
#define     SCREEN_Height           ([[UIScreen mainScreen] bounds].size.height)
#define     StatusBarHeight         ([UIApplication sharedApplication].statusBarFrame.size.height)
#define     NavigationHeight        (StatusBarHeight + 44)
#define     TabBarHeight            (StatusBarHeight == 44 ? 83 : 49)
#define     TopSafeAreaHeight       (StatusBarHeight - 20)
#define     BottomSafeAreaHeight    (TabBarHeight - 49)
#define     ZoomSize(S)             (S *(SCREEN_Width)/375)


#import "AppDelegate.h"
#import <WebKit/WebKit.h>
#import <AVFoundation/AVFoundation.h>

#import "MBProgressHUD.h"
#import "NTWebViewViewController.h"

#import "ViewController.h"

#import "DDQRCodeViewController.h"
#import "DDPhotoQRCodeViewController.h"

@interface ViewController ()<WKNavigationDelegate,WKScriptMessageHandler>

@property (nonatomic, strong) WKWebView                *webView;
@property (nonatomic, strong) UIActivityIndicatorView  *indicatorView;
@property (nonatomic, strong)  AppDelegate             *appdelegate;

@end

@implementation ViewController

- (UIActivityIndicatorView *)indicatorView {
    if (!_indicatorView) {
        _indicatorView = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        _indicatorView.center = self.view.center;
        _indicatorView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
        [self.view addSubview:_indicatorView];
    }
    return _indicatorView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    _appdelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    UIView *statusBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, StatusBarHeight)];
    statusBarView.backgroundColor = [UIColor colorWithRed:60/255.0 green:176/255.0 blue:105/255.0 alpha:1];
    [self.view addSubview:statusBarView];
    
    //设置监听
    WKUserContentController* userContentController = [[WKUserContentController alloc] init];
    [userContentController addScriptMessageHandler:self name:@"app"];
    // 设置偏好设置
    WKWebViewConfiguration *config = [[WKWebViewConfiguration alloc] init];
    config.userContentController = userContentController;
    
    _webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, StatusBarHeight,  SCREEN_Width, SCREEN_Height - StatusBarHeight ) configuration:config];
    if (@available(iOS 11.0, *)) {
        self.webView.scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    _webView.scrollView.bounces = NO;
    _webView.navigationDelegate = self;
    _webView.scrollView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:_webView];

    [self loadRequest];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetwork) name:@"networkNotification" object:nil]; //编辑用户设置
    
}

- (void) loadRequest {
    //页面加载逻辑
    NSString* urlString = [NSString stringWithFormat:@"http://gn.hs620.cn/weixin/toLogin.do?visitType=1"];
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlString]]];
}

- (void) checkNetwork {
    
    NetworkStatus status = _appdelegate.reachability.currentReachabilityStatus;
    if (status == NotReachable) {
//        [self tipNoNetwork];
    } else {
        [self loadRequest];
    }
}

#pragma mark - WKScriptMessageHandler
- (void)userContentController:(WKUserContentController *)userContentController
      didReceiveScriptMessage:(WKScriptMessage *)message {
    
    //    NSLog(@"message.name %@",message.name);
    //    NSLog(@"message.body %@",message.body);
    
    NSDictionary *dic = message.body;
    NSLog(@"dic %@",dic);
    
    //获取id方法
    if ([dic[@"function"] isEqualToString:@"getYijiawangUserId"] ) {
        NSString *userid = dic[@"content"];
        [[NSUserDefaults standardUserDefaults] setObject:userid forKey:@"userid"];
    }
    
    //打开外部链接方法
    else if ([dic[@"function"] isEqualToString:@"openUrl"] ) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:dic[@"content"]]];
    }
    
    else if ([dic[@"function"] isEqualToString:@"getScanCode"] ) {
//        NTWebViewViewController *ActivityVC = [[NTWebViewViewController alloc] init];
//        ActivityVC.url = @"http://www.baidu.com";
//        [self.navigationController pushViewController:ActivityVC animated:YES];
        
        if ([self validateCamera] && [self canUseCamera]) {
//            [self showQRViewController:];
            [self showQRViewController:dic[@"content"]];
        } else {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"没有摄像头或摄像头不可用" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView show];
        }
        
    }
}

-(BOOL)canUseCamera {
    
    NSString *mediaType = AVMediaTypeVideo;
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:mediaType];
    if(authStatus == AVAuthorizationStatusRestricted || authStatus == AVAuthorizationStatusDenied){
        
        NSLog(@"相机权限受限");
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"请在设备的设置-隐私-相机中允许访问相机。" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alertView show];
        return NO;
    }
    
    return YES;
}

-(BOOL)validateCamera {
    
    return [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] &&
    [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear];
}


- (void)showQRViewController:(NSString *) getContent {
    
    DDQRCodeViewController *vc = [[DDQRCodeViewController alloc] initWithScanCompleteHandler:^(NSString *url) {
        NSLog(@"url");
        
        NSString* urlString = [NSString stringWithFormat:@"http://gn.hs620.cn%@%@",getContent,url];
        [self->_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlString]]];
        
    }];
    vc.contentString = getContent;
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)showPhotoQRViewController {
    
    DDPhotoQRCodeViewController *vc = [[DDPhotoQRCodeViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}


//如果不实现这个代理方法,默认会屏蔽掉打电话等url

- (void) webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    decisionHandler(WKNavigationActionPolicyAllow);
}


/// 2 页面开始加载
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation{
    NSLog(@"1");
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.detailsLabel.text = nil;
    [hud hideAnimated:YES afterDelay:5];
    
    //添加检查网络方法
    NetworkStatus status = _appdelegate.reachability.currentReachabilityStatus;
    [self showHUDWithReachabilityStatus:status];
    
    //关于拨打电话时的调用问题
    NSString *path= [webView.URL absoluteString];
    NSString * newPath = [path lowercaseString];
    
    if ([newPath hasPrefix:@"sms:"] || [newPath hasPrefix:@"tel:"]) {
        UIApplication * app = [UIApplication sharedApplication];
        if ([app canOpenURL:[NSURL URLWithString:newPath]]) {
            [app openURL:[NSURL URLWithString:newPath]];
        }
        
        [self.indicatorView stopAnimating];
        return;
    }
}

/// 4 开始获取到网页内容时返回
- (void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation {
    NSLog(@"2");
}

/// 5 页面加载完成之后调用
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
    NSLog(@"3");
    //    [self.indicatorView stopAnimating];
    [MBProgressHUD hideHUDForView:self.view animated:true];
}

/// 页面加载失败时调用
- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation{
    NSLog(@"4");
    [MBProgressHUD hideHUDForView:self.view animated:true];
}

#pragma mark-- 执行网络判断
- (void)showHUDWithReachabilityStatus:(NetworkStatus)status {
    if (status == NotReachable) {
        
        [MBProgressHUD hideHUDForView:self.view animated:true];
        
//        UIAlertView *alterView = [[UIAlertView alloc] initWithTitle:nil message:@"网络已断开，请检查网络！" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil];
//        [alterView show];
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
