//
//  AppDelegate.h
//  GuanNanFinance
//
//  Created by steve on 2019/1/7.
//  Copyright © 2019 GuanNanFinance. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) Reachability *reachability;


@end

